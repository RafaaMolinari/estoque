from django.db import models

from django.contrib.auth.models import User

# Create your models here.

class Product(models.Model):
     name = models.CharField(max_length=45)
     description = models.TextField()
     code = models.CharField(max_length=12)
     image = models.ImageField(null=True, upload_to = 'products')
     amount = models.PositiveIntegerField()


     def __str__(self):
        return self.code + ' - ' + self.name
