from django.views.generic import ListView, UpdateView, DeleteView
from stock.models import Product
from django.shortcuts import redirect


class ProductListView(ListView):
    model = Product

class OutOfStockListView(ListView):
    model = Product

    def get_queryset(self):
        products = super().get_queryset()
        return products.filter(amount=0)

class AddStockView(UpdateView):
    model = Product
    fields = ['amount']


    def post(self,request, *args, **kwargs):
        product = self.get_object()
        amount = self.request.POST['amount']
        product.amount+= int(amount)
        product.save()
        return redirect('product-list')
        

class RemoveStockView(DeleteView):
    model = Product
    fields = ['amount']


    def post(self,request, *args, **kwargs):
        product = self.get_object()
        amount = self.request.POST['amount']
        product.amount -= int(amount)
        product.save()
        return redirect('product-list')
